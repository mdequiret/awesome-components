import { AfterViewInit, Directive, ElementRef, HostListener, Input, Renderer2 } from "@angular/core";

@Directive({
    selector: '[highlight]'
})
export class HighLightDirective  implements AfterViewInit{
    
    @Input() highlight!: string;
    constructor(private el: ElementRef, private renderer: Renderer2) {}

    ngAfterViewInit(){
        this.setBackgroundColor(this.highlight)
    }
    setBackgroundColor(color: string) {
        this.renderer.setStyle(this.el.nativeElement, 'background-color', color)
    }

    @HostListener('mouseenter')
    onMouseEnter() {
        this.setBackgroundColor('lightGreen')
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        this.setBackgroundColor(this.highlight)
    }

    @HostListener('click')
    onClick() {
        this.highlight = 'lightGreen'

    }
}

// @Directive({
//     selector: '[highlight]'
// })
// export class HighLightDirective  implements AfterViewInit{
    
//     @Input() color: string = 'yellow'
//     constructor(private el: ElementRef, private renderer: Renderer2) {}

//     ngAfterViewInit(){
//         this.setBackgroundColor(this.color)
//     }
//     setBackgroundColor(color: string) {
//         this.renderer.setStyle(this.el.nativeElement, 'background-color', color)
//     }
// }

// Dans ce cas on peut affecter une valeur par défaut au Input et doit utiliser la directive tel que :
// <span highlight color="red">
// 