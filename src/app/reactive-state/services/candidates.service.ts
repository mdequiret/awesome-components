import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, delay, filter, find, map, Observable, switchMap, take, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { Candidate } from "../models/candidate.model";

@Injectable()
export class CandidatesService {

    private _loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
    private _candidates$: BehaviorSubject<Candidate[]> = new BehaviorSubject<Candidate[]>([])
    private _lastCandidatesLoad: number = 0;

    constructor(private http: HttpClient) { }

    // getLoading() Observable<boolean> {
    //     return this._loading$.asObservable();
    // }
    get loading$(): Observable<boolean> {
        return this._loading$.asObservable();
    }
    private setLoadingStatus(isLoading: boolean): void {
        return this._loading$.next(isLoading);
    }

    
    get candidates$(): Observable<Candidate[]> {
        return this._candidates$.asObservable();
    }
    private setCandidates(candidates: Candidate[]): void {
        return this._candidates$.next(candidates);
    }

    getCandidatesFromServer(): void {
        if(Date.now() - this._lastCandidatesLoad <= 1000*60*5) {
            return
        } else {
            this.setLoadingStatus(true)
            this.http.get<Candidate[]>(`${environment.apiUrl}/candidates`).pipe(
                delay(1000),
                tap(candidates => {
                    this._lastCandidatesLoad = Date.now();
                    this.setCandidates(candidates);
                    this.setLoadingStatus(false)
                })
            ).subscribe();
        }
    }

    getCandidateById(id: number): Observable<Candidate> {
        if(!this._lastCandidatesLoad) {
            this.getCandidatesFromServer();
        }
        return this.candidates$.pipe(
            map(candidates => candidates.filter(candidate => candidate.id === id)[0])
        )
    }

    refuseCandidate(id: number): void {
        this.setLoadingStatus(true);
        this.http.delete(`${environment.apiUrl}/candidates/${id}`).pipe(
            delay(1000),
            switchMap(() => this.candidates$),
            take(1),
            map(candidates => candidates.filter(candidate => candidate.id !== id)),
            tap(candidates => {
                this._candidates$.next(candidates);
                this.setLoadingStatus(false)
            })
        ).subscribe()
    }

    hireCandidate(id: number): void {
        this.candidates$.pipe(
            take(1),
            map(candidates => candidates.map(candidate => candidate.id === id ? {...candidate, company: 'Snapface Ltd'} : candidate)),
            tap(updateCandidates => this._candidates$.next(updateCandidates)),
            switchMap(updateCandidates => this.http.patch(`${environment.apiUrl}/candidates/${id}`, updateCandidates.find(candidate => candidate.id === id)))
        ).subscribe();
    }
}
