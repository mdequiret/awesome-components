import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { combineLatest, map, Observable, startWith } from 'rxjs';
import { CandidateSearchTypeEnum } from '../../enums/candidate-search-type.enum';
import { Candidate } from '../../models/candidate.model';
import { CandidatesService } from '../../services/candidates.service';

@Component({
  selector: 'app-candidate-list.component.ts',
  templateUrl: './candidate-list.component.ts.component.html',
  styleUrls: ['./candidate-list.component.ts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandidateListComponent implements OnInit {


  loading$!: Observable<boolean>;
  candidates$!: Observable<Candidate[]>;

  searchCtrl!: FormControl;
  searchTypeCtrl!: FormControl;
  searchTypeOptions!: {
    value: CandidateSearchTypeEnum,
    label: string
  }[];

  constructor(private candidatesService: CandidatesService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.initObservables();
    this.candidatesService.getCandidatesFromServer();
  }

  private initForm() {
    this.searchCtrl = this.formBuilder.control('')
    this.searchTypeCtrl = this.formBuilder.control(CandidateSearchTypeEnum.LASTNAME)
    this.searchTypeOptions = [ 
      {
        value: CandidateSearchTypeEnum.LASTNAME,
        label: 'Nom'
      },{
        value: CandidateSearchTypeEnum.FIRSTNAME,
        label: 'Prénom'
      },{
        value: CandidateSearchTypeEnum.COMPANY,
        label: 'Entreprise'
      }
    ]
  } 
  
  private initObservables() {
    this.loading$ = this.candidatesService.loading$
    const search$ = this.searchCtrl.valueChanges.pipe(
      startWith(this.searchCtrl.value),
      map(value => value.toLowerCase())
    )
    const searchType$: Observable<CandidateSearchTypeEnum> = this.searchTypeCtrl.valueChanges.pipe(
      startWith(this.searchTypeCtrl.value)
    )
    this.candidates$ = combineLatest([
      search$,
      searchType$,
      this.candidatesService.candidates$
    ]).pipe(
      map(([search, searchType, candidates]) => candidates.filter(candidate => candidate[searchType]
        .toLowerCase()
        .includes(search as string)))
    )
  }
}
