import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveStateRoutingModule } from './reactive-state-routing.module';
import { CandidateListComponent } from './components/candidate-list.component.ts/candidate-list.component.ts.component';
import { SingleCandidateComponent } from './components/single-candidate.component.ts/single-candidate.component.ts.component';
import { CandidatesService } from './services/candidates.service';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    CandidateListComponent,
    SingleCandidateComponent,
  ],
  imports: [
    CommonModule,
    ReactiveStateRoutingModule,
    SharedModule
  ],
  providers: [
    CandidatesService
  ]
})
export class ReactiveStateModule { }
