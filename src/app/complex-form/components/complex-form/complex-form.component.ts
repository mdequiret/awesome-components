import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { map, Observable, of, startWith, tap } from 'rxjs';
import { ComplexFormService } from '../../services/complex-form.service';
import { confirmEqualValidator } from '../../validators/confirm-equal.validator';
import { validValidator } from '../../validators/valid.validator';

@Component({
  selector: 'app-complex-form',
  templateUrl: './complex-form.component.html',
  styleUrls: ['./complex-form.component.scss']
})
export class ComplexFormComponent implements OnInit {

  loading: boolean = false;
  mainForm!: FormGroup;
  personalInfoForm!: FormGroup;
  contactPreferenceCtrl!: FormControl;
  phoneCtrl!: FormControl;

  emailCtrl!: FormControl;
  confirmEmailCtrl!: FormControl;
  emailForm!: FormGroup;

  passwordCtrl!: FormControl;
  confirmPasswordCtrl!: FormControl;
  loginInfoForm!: FormGroup;

  loginInfoCtrl!: FormControl;
  confirmLoginInfoCtrl!: FormControl;

  showEmailCtrl$!: Observable<boolean>;
  showPhoneCtrl$!: Observable<boolean>;
  showEmailError$!: Observable<boolean>;
  showPasswordError$!: Observable<boolean>;

  constructor(private formBuilder: FormBuilder, private complexFormService: ComplexFormService) { }

  ngOnInit(): void {
    this.initFormControl();
    this.initMainForm();
    this.initFormObservable();
  }

  private initMainForm(): void {
    this.mainForm = this.formBuilder.group({
      personalInfo: this.personalInfoForm,
      contactPreference: this.contactPreferenceCtrl,
      email: this.emailForm,
      phone: this.phoneCtrl,
      loginInfo: this.loginInfoForm
    })
  }

  private initFormControl() {
    this.personalInfoForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    })
    this.contactPreferenceCtrl = this.formBuilder.control('email');

    this.emailCtrl = this.formBuilder.control('', Validators.required);
    this.confirmEmailCtrl = this.formBuilder.control('', Validators.required);
    this.emailForm = this.formBuilder.group({
      email: this.emailCtrl,
      confirm: this.confirmEmailCtrl
    }, {
      validators: [confirmEqualValidator('email', 'confirm')],
      updateOn: 'blur'
    });

    this.phoneCtrl = this.formBuilder.control('');

    this.passwordCtrl = this.formBuilder.control('', Validators.required);
    this.confirmPasswordCtrl = this.formBuilder.control('', Validators.required);
    this.loginInfoForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: this.passwordCtrl,
      confirmPassword: this.confirmPasswordCtrl
    }, {
      validators: [confirmEqualValidator('password', 'confirmPassword')],
      updateOn: 'blur'
    });
  }

  initFormObservable() {
    this.showEmailCtrl$ = this.contactPreferenceCtrl.valueChanges.pipe(
      startWith(this.contactPreferenceCtrl.value),
      map(preference => preference === 'email'),
      tap(showEmailCtrl => this.setEmailValidators(showEmailCtrl))
    );
    this.showPhoneCtrl$ = this.contactPreferenceCtrl.valueChanges.pipe(
      startWith(this.contactPreferenceCtrl.value),
      map(preference => preference === 'phone'),
      tap(showPhoneCtrl => this.setPhoneValidators(showPhoneCtrl))
    );
    this.showEmailError$ = this.emailForm.statusChanges.pipe(
      map(status => status === 'INVALID' && this.emailCtrl.value && this.confirmEmailCtrl.value && this.emailForm.hasError('confirmEqual'))
    );
    this.showPasswordError$ = this.loginInfoForm.statusChanges.pipe(
      map(status => status === 'INVALID' && this.passwordCtrl.value && this.confirmPasswordCtrl.value && this.loginInfoForm.hasError('confirmEqual'))
    );
  }

private setEmailValidators(showEmailCtrl: boolean): void {
  if (showEmailCtrl) {
    this.emailCtrl.addValidators([Validators.required, Validators.email, validValidator()]);
    this.confirmEmailCtrl.addValidators([Validators.required, Validators.email]);
  } else {
    this.emailCtrl.clearValidators();
    this.confirmEmailCtrl.clearValidators();
  }
  this.emailCtrl.updateValueAndValidity();
  this.confirmEmailCtrl.updateValueAndValidity();
}

private setPhoneValidators(showPhoneCtrl: boolean): void {
  if (showPhoneCtrl) {
    this.phoneCtrl.addValidators([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]);
  } else {
    this.phoneCtrl.clearValidators();
  }
  this.phoneCtrl.updateValueAndValidity();
}

getFormControlText(ctrl: AbstractControl) {
  if(ctrl.hasError('required')) {
    return 'Ce champ est requis.';
  }
  else if(ctrl.hasError('email')) {
    return 'Ce champ doit être une adresse email valide.';
  }
  else if(ctrl.hasError('validValidator')) {
    return 'Ce champ doit contenir .com';
  }
  else if(ctrl.hasError('pattern')) {
    return 'Ce champ ne doit contenir que des chiffres.';
  }
  else if(ctrl.hasError('minlength') || ctrl.hasError('maxlength')) {
    return 'Ce champ doit faire exactement 10 caractères.';
  }
  return 'Ce champ contient une erreur.';
}

  onSubmitForm() {
    this.loading = true;
    this.complexFormService.saveUserInfo(this.mainForm.value).pipe(
      tap(saved => {
        this.loading = false;
        if(saved) {
          this.resetForm();
        } else {
            console.error('Echec de l\'enregistrement.')
        }
      })
    ).subscribe();
  }

  private resetForm() {
    this.mainForm.reset();
    this.contactPreferenceCtrl.patchValue('email'); // déclenche une émission pour l'observable
  }

}

