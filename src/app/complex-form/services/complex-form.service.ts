import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, mapTo, delay, catchError, of } from "rxjs";
import { environment } from "src/environments/environment";
import { ComplexFormValue } from "../models/complex-form-value.models";

@Injectable()
export class ComplexFormService {
    constructor(private http: HttpClient) {

    }

    saveUserInfo(formValue: ComplexFormValue) : Observable<boolean> {
        return this.http.post(`${environment.apiUrl}/users`, formValue).pipe(
            mapTo(true), // Si le server répond on envoie true, qu'importe ce qu'il répond.
            delay(1000),
            catchError(() => of(false).pipe(
                delay(1000)
            ))
        );
    }
}